package com.hcl.util;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class ConnectionData {
    static Connection dbConn = null;
    public static Connection getConnection() throws ClassNotFoundException {
        try {
            Properties props = new Properties();
            String dbSettingsPropertyFile = "C:\\SpringBootPrograms\\Cucumber_Java\\src\\main\\resources\\db.properties";
            FileReader fReader = new FileReader(dbSettingsPropertyFile);
            props.load(fReader);
            String dbDriverClass = props.getProperty("db.driver.class");
            String dbConnUrl = props.getProperty("db.conn.url");
            String dbUserName = props.getProperty("db.username");
            String dbPassword = props.getProperty("db.password");
            Class.forName(dbDriverClass);
            dbConn = DriverManager.getConnection(dbConnUrl, dbUserName, dbPassword);
        } catch (Exception e) {
            System.out.println(e);
        }
        return dbConn;
    }
}
