package com.hcl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import com.hcl.dao.EmployeeDao;
import com.hcl.main.Main;
import com.hcl.model.Employee;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class DeleteEmployee {
    Main main;
    private int id;
   
    List<String> list;
       @Given("^User wants to delete an employee detail$")
    public void user_wants_to_delete_an_employee_detail()throws Throwable {
           main = new Main();
           
    }

    @When("^User enters the id as (\\d+)$")
    public void user_enter_the_id_as(Integer id) throws Throwable {
        this.id = id;
       
        
    }
   /* @And("^Fetch data from database for deletion$")
    public void fetch_data_from_database_for_deletion(DataTable dt)
        {
      list = dt.asList(String.class);
        //list.clear();
        }*/
    @Then("^Data successfully deleted and it returns as (\\d+)$")
    public void data_successfully_deleted_and_it_returns_as(int result) throws Throwable {
        Assert.assertEquals(result,main.delete(id));
       // Assert.assertEquals(0,Integer.parseInt(list.get(0)));
        
    }
}
